reset

set terminal epslatex color
set output "viskusteig.tex"

set xlabel "Zeit [s]"
set ylabel "$\ln$(Höhe) [$\ln$(M)]"

f(x)=m*x+b

set fit logfile "viskusteig.log"
fit f(x) "viskusteid.txt" using 2:(log($1)):3 via m,b

p f(x) t "linear Fit", "viskusteid.txt" using 2:(log($1)):3 w e t "Messwerte"
#!epstopdf viskusteig.eps
